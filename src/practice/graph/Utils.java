package prontidis.practice.adts.graph;

import java.util.*;

public class Utils {
    public static<T> void BFS(Graph<T> graph, Node<T> source) {
        int V = graph.vertices.size();

        Map<Node<T>, Integer> state = new HashMap<>();
        for (Node<T> node : graph.vertices) {
            if (!node.equals(source)) {
                state.put(node, -1);
            }
        }

        Queue<Node<T>> q = new ArrayDeque<>();
        q.add(source);

        while (!q.isEmpty()) {
            Node<T> current = q.poll();
            System.out.print(current.data.toString() + " ");

            for (Edge<T> edge : current.outEdges) {
                Node<T> node = edge.to;
                if (state.get(node) == -1) {
                    state.put(node, 0);
                    q.add(node);
                }
            }
            state.put(current, 1);
        }

        System.out.print("\n");
    }

    private static<T> void DFS_visit(Node<T> source, Map<Node<T>, Integer> state) {
        state.put(source, 0);
        System.out.print(source.data.toString() + " ");

        for (Edge<T> edge : source.outEdges) {
            Node<T> node = edge.to;
            if (state.get(node) == -1) {
                DFS_visit(node, state);
            }
        }
    }

    public static<T> void DFS(Graph<T> graph) {
        int V = graph.vertices.size();

        Map<Node<T>, Integer> state = new HashMap<>();
        for (Node<T> node : graph.vertices) {
                state.put(node, -1);
        }

        for (Node<T> node : graph.vertices) {
            if (state.get(node) == -1) {
                DFS_visit(node, state);
            }
        }
        System.out.print("\n");
    }

    public static<T> void topologicalSort(Graph<T> graph) {

        //L <- Empty list that will contain the sorted elements
        ArrayList<Node<T>> L = new ArrayList<>();

        //S <- Set of all nodes with no incoming edges
        HashSet<Node<T>> S = new HashSet<>();
        for (Node<T> n : graph.vertices) {
            if (n.inEdges.size() == 0) {
                S.add(n);
            }
        }

        //while S is non-empty do
        while (!S.isEmpty()) {
            //remove a node n from S
            Node<T> n = S.iterator().next();
            S.remove(n);

            //insert n into L
            L.add(n);

            //for each node m with an edge e from n to m do
            for (Iterator<Edge> it = n.outEdges.iterator(); it.hasNext(); ) {
                //remove edge e from the graph
                Edge e = it.next();
                Node<T> m = e.to;

                it.remove();//Remove edge from n
                m.inEdges.remove(e);//Remove edge from m

                //if m has no other incoming edges then insert m into S
                if (m.inEdges.isEmpty()) {
                    S.add(m);
                }
            }
        }
        //Check to see if all edges are removed
        boolean cycle = false;
        for (Node<T> n : graph.vertices) {
            if (!n.inEdges.isEmpty()) {
                cycle = true;
                break;
            }
        }
        if (cycle) {
            System.out.println("Cycle present, topological sort not possible");
        } else {
            System.out.println("Topological Sort: " + Arrays.toString(L.toArray()));
        }
    }
}
