package prontidis.practice.adts.graph;

import java.util.HashSet;

public class Node<T>{
    public final T data;
    public final HashSet<Edge> inEdges;
    public final HashSet<Edge> outEdges;

    public Node(T data) {
        this.data = data;
        inEdges = new HashSet<Edge>();
        outEdges = new HashSet<Edge>();
    }

    public Node addEdge(Node node){
        Edge e = new Edge(this, node);
        outEdges.add(e);
        node.inEdges.add(e);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return data.equals(node.data);
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    @Override

    public String toString() {
        return data.toString();
    }
}