package prontidis.practice.adts.graph;

class Edge<T>{
    public final Node<T> from;
    public final Node<T> to;

    public Edge(Node<T> from, Node<T> to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object obj) {
        Edge e = (Edge)obj;
        return e.from.equals(from) && e.to.equals(to);
    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        return result;
    }
}
