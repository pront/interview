package prontidis.practice.adts.graph;

import javax.rmi.CORBA.Util;
import java.util.*;

public class Graph<T> {

    Set<Node<T>> vertices = new HashSet<Node<T>>();

    Graph<T> addVertex(Node<T> n) {
        vertices.add(n);
        return this;
    }

    public static void main(String[] args) {
        Node<Integer> seven = new Node<>(7);
        Node<Integer> five = new Node<>(5);
        Node<Integer> three = new Node<>(3);
        Node<Integer> eleven = new Node<>(11);
        Node<Integer> eight = new Node<>(8);
        Node<Integer> two = new Node<>(2);
        Node<Integer> nine = new Node<>(9);
        Node<Integer> ten = new Node<>(10);
        seven.addEdge(eleven).addEdge(eight);
        five.addEdge(eleven);
        three.addEdge(eight).addEdge(ten);
        eleven.addEdge(two).addEdge(nine).addEdge(ten);
        eight.addEdge(nine).addEdge(ten);

        Graph<Integer> g = new Graph<Integer>();
        g.addVertex(seven).
          addVertex(five).
          addVertex(three).
          addVertex(eleven).
          addVertex(eight).
          addVertex(two).
          addVertex(nine).
          addVertex(ten);

        Utils.BFS(g, eleven);
        Utils.DFS(g);
        Utils.topologicalSort(g);
    }
}