package prontidis.practice.adts;

import java.util.*;

public class SimpleGraph {
    private static class Node {
        int id;
        int weight = 1;
        Map<Integer, Node> adj = new HashMap<>();

        Node(int id) {
            this.id = id;
        }

        Node(int id, int weight) {
            this.id = id;
            this.weight = weight;
        }

        // could use a hashMap and avoid iteration
        public boolean addLink(Node n) {
            if (adj.get(n.id) != null) {
                return false;
            }
            adj.put(n.id, n);
            return true;
        }
    }

    Map<Integer, Node> nodes = new HashMap<>();
    boolean directed = true;

    SimpleGraph() { }

    SimpleGraph(boolean directed) {
        this.directed = directed;
    }

    public void addNode(int id) {
        nodes.put(id, new Node(id));
    }

    public Node getNode(int id) {
        return nodes.get(id);
    }
    public boolean addEdge(int u, int v) {
        Node n1 = getNode(u);
        Node n2 = getNode(v);

        if (n1 == null || n2 == null) {
            return false;
        }

        boolean added = n1.addLink(n2);
        if (!this.directed) {
            return added && n2.addLink(n1);
        }
        return added;
    }

    enum State { UNVISITED, VISITING, VISITED }

    public void bfs() {
        if (nodes.size() == 0) {
            System.out.println("Empty graph");
            return;
        }

        State[] state = new State[nodes.size()];
        for (Integer i : nodes.keySet()) {
            state[i] = State.UNVISITED;
        }

        Queue<Integer> q = new LinkedList<>();

        Iterator<Integer> it = nodes.keySet().iterator();
        int curId = it.next();
        state[curId] = State.VISITING;
        q.add(curId);

        while(!q.isEmpty()) {
            curId = q.poll();
            Node n = nodes.get(curId);
            for (Integer i : n.adj.keySet()) {
                state[i] = State.VISITING;
                q.add(i);
            }
            state[curId] = State.VISITED;
            System.out.println("Visited " + curId);
        }
    }

    private boolean dfs() {
        if (nodes.size() == 0) {
            System.out.println("Empty graph");
            return false;
        }

        Stack<Integer> stack = new Stack<>();
        State[] state = new State[nodes.size()];
        for (Integer i : nodes.keySet()) {
            state[i] = State.UNVISITED;
        }

        for (Integer i : nodes.keySet()) {
            if (dfsVisit(i, state, stack)) {
                return true;
            }
        }

        System.out.print("Topological sort: ");
        while(!stack.isEmpty()) {
            System.out.print(stack.pop() + " ");
        }
        System.out.println();
        return false;
    }

    public boolean dfsVisit(int id, State[] state, Stack<Integer> stack) {
        if (state[id] == State.VISITED) {
            return false;
        }

        state[id] = State.VISITING;
        System.out.println("Visiting " + id);
        Node node = nodes.get(id);
        for (Integer i : node.adj.keySet()) {
            if (state[i] == State.VISITING || dfsVisit(i, state, stack)) {
                return true;
            }
        }
        state[id] = State.VISITED;
        System.out.println("Visited " + id);
        stack.push(id);
        return false;
    }

    public static void main(String[] args) {
        SimpleGraph g = new SimpleGraph();
        int n = 10;
        for (int i = 0; i < n; ++i) {
            g.addNode(i);
        }

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(0, 3);
        g.addEdge(1, 4);
        g.addEdge(1, 5);
        g.addEdge(4, 6);
        g.addEdge(4, 7);
        g.addEdge(4, 8);
        g.addEdge(8, 9);

        g.bfs();
    }
}
