package prontidis.practice.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.jar.Pack200;

// N queens
// N x N board
// find all possible ways to place queens on the board
public class ChessQueenPlacement {
    final int N;
    int[] columns;
    ArrayList<int[]> results;

    ChessQueenPlacement(int N) {
        this.N = N;
        this.columns = new int[N];
        Arrays.fill(columns, 0);

        results = new ArrayList<>();
    }

    private boolean checkIfValid(int row, int column) {
        for (int r2 = 0; r2 < row; r2++) {
            int c2 = this.columns[r2];
            if (column == c2) { return false; } // same column

            int colDistance = Math.abs(c2 - column);
            int rowDistance = Math.abs(r2 - row);
            if (rowDistance == colDistance) { return false; } // same diagonal
        }

        return true;
    }

    public void placeQueens() {
        placeQueens(0);
    }

    private void placeQueens(int row) {
        if (row == N) {
            results.add(columns.clone());
        }
        else {
            for (int col = 0; col < N; col++) {
                if (checkIfValid(row, col)) {
                    columns[row] = col;
                    placeQueens(row + 1);
                }
            }
        }
    }

    private String placementToString(int[] placement) {
        StringBuilder board = new StringBuilder(2 * N * N + N);
        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++) {
                board.append(placement[row] == col ? "1 " : "0 ");
            }
            board.append("\n");
        }
        return board.toString();
    }

    public void printResults() {
        for (int[] placement : results) {
            System.out.println(placementToString(placement) + "\n");
        }
    }

    public static void main(String[] args) {
        ChessQueenPlacement q8 = new ChessQueenPlacement(8);
        q8.placeQueens();
        q8.printResults();
    }
}
