package prontidis.practice.problems;

import prontidis.practice.utils.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by prontidis on 8/13/16.
 */
public class GenerateParenthesisSequences {
    static private void addParen(ArrayList<String> result, int lcount, int rcount, char[] str, int idx) {
        if (lcount == 0 && rcount == 0) {
            result.add(new String(str, 0, idx));
            return;
        }

        if (lcount > 0) {
            str[idx] = '(';
            addParen(result, lcount - 1, rcount, str, idx + 1);
        }

        if (rcount > 0 && rcount > lcount) {
            str[idx] = ')';
            addParen(result, lcount, rcount - 1, str, idx + 1);
        }

    }

    static public ArrayList<String> genParen(int n) {
        ArrayList<String> result = new ArrayList<>();
        if (n <= 0) {
            return result;
        }
        char[] str = new char[2 * n];
        addParen(result, n, n, str, 0);
        return result;
    }

    public static void main(String[] args) {
        ArrayList<String> result = genParen(3);
        for (String s : result) {
            System.out.println(s);
        }
    }


}
